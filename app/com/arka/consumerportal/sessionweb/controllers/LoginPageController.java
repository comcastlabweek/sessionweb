/**
 * 
 */
package com.arka.consumerportal.sessionweb.controllers;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.services.PolicyService;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.PropUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

public class LoginPageController extends Controller {
	
	public static final Logger logger = LoggerFactory.getLogger(LoginPageController.class);
	
	@Inject
	PolicyService policyService;
	
	@Inject
	MandatoryParams mandatoryParams;
	
	@Inject
	PropUtils propUtils;
	
	public Result loginAs() {
		Map<String, String[]> requestBody = request().body().asFormUrlEncoded();
		String[] userTypes = requestBody.get("userType");
		if(userTypes != null && userTypes.length > 0) {
			String userType = userTypes[0];
			if(userType.equals("1") || userType.equals("2")) {
				String encryptUserType = CryptUtils.encryptData(userType);
				session("userType", encryptUserType);
			}
			if(userType.equals("1")) {
				return redirect("/policy/consumer/get/policy/");
			}
			if(userType.equals("2")) {
				return redirect("/policy/consumer/manage/policy/");
			}
		}
//		JsonNode jsonNode = request().body().asJson();
//		JsonNode userType = jsonNode.get("userType");
//		if(userType == null) {
//			return badRequest();
//		} else {
//			try {
//				Map<String, String> cookies = CookieUtils.getCookies("USER_SESSION", request());
//				if(cookies == null)
//					cookies = new HashMap<>();
//				cookies.put("user_type", userType.textValue());
//				CookieUtils.setCookies("USER_SESSION", cookies, response());
//			} catch (Exception e) {
//				return internalServerError();
//			}
//		}
		return redirect("/session/user/choosetype");
	}

	public CompletionStage<Result> LoginPage() {
		ObjectNode responseJson = Json.newObject();
		// session("sid", responseJson.get("sessionId").asText());
		return CompletableFuture.completedFuture(
				ok(com.arka.consumerportal.sessionweb.views.html.loginPage.render(responseJson, propUtils)));
	}

	public Result chooseUserType() {
		
		if (session("userType") != null) {
			Long userType = CryptUtils.getDecryptLongData(session("userType"));
			if(userType == 1l) {
				return redirect("/policy/consumer/get/policy/");
			}
			if(userType == 2l) {
				return redirect("/policy/consumer/manage/policy/");
			}
		}
		Map<String, List<String>> mandatQP = CriteriaUtils.queryStringArrayToList(request().queryString());
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		String uid = session(SessionTrackingParams.USER_ID.getValue());

		logger.debug("loading policies fro userId : " + uid);

		if(uid == null) {
			return redirect("/session/login");
		}
			
		
		try{
			if(session("isCorpUser") == null) {
				JsonNode statusNode = policyService.isCorporateUser(uid, mandatQP, mandatHP).toCompletableFuture().get();
	
				if (statusNode.get("status") != null) {
					session("isCorpUser", statusNode.get("status").asText());
					if (!statusNode.get("status").asBoolean()) {
						return redirect("/policy/consumer/manage/policy/");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ObjectNode responseJson = Json.newObject();
		return ok(com.arka.consumerportal.sessionweb.views.html.userTypeRedirectPage.render(responseJson, propUtils));
	}

}
