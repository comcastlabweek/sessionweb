package com.arka.consumerportal.sessionweb.controllers;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jose4j.json.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.RedisDB;
import com.arka.common.constants.SessionTrackingParams;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.services.ProfileService;
import com.arka.common.services.SessionServ;
import com.arka.common.user.constants.UserAuthField;
import com.arka.common.user.constants.UserField;
import com.arka.common.user.constants.UserGroupField;
import com.arka.common.user.constants.UserProfileField;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.consumerportal.sessionweb.messagekey.utils.UserMessageKey;
import com.arka.consumerportal.sessionweb.services.LoginService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Http.RequestBody;
import play.mvc.Result;

public class ConsumerUserController extends Controller {

	public static final Logger logger = LoggerFactory.getLogger(ConsumerUserController.class);

	@Inject
	HttpExecutionContext executionContext;

	@Inject
	LoginService loginService;

	@Inject
	SessionServ sessionServ;

	@Inject
	PropUtils propUtils;

	@Inject
	MessagesApi messagesApi;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	FormFactory formFactory;

	@Inject
	ProfileService profileService;

	@Inject
	private CacheService cache;

	public CompletionStage<Result> generateUniqueId() {
		String source = request().getQueryString("source") != null ? request().getQueryString("source") : null;
		if (StringUtils.isNotEmpty(source)) {

			ObjectNode visitorIdJson = JsonUtils.newJsonObject();
			Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
			visitorIdJson.put("source", source);
			return loginService.generateUniqueId(mandatQP, mandatHP, visitorIdJson)
					.thenApplyAsync(respJson -> ok(respJson), executionContext.current());
		} else {
			return CompletableFuture.completedFuture(badRequest(
					JsonUtils.getJsonresponseForBadRequest(new ValidationError("source", "INVALID SOURCE"))));
		}
	}

	public CompletionStage<Result> getUserDetails() {

		ObjectNode consumerUserJson = JsonUtils.newJsonObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		consumerUserJson.put("source", "getuser");

		return loginService.getUser(mandatQP, mandatHP, consumerUserJson).thenApplyAsync(userJson -> {
			logger.debug("user json from session : " + userJson);
			return ok(userJson);
		});
	}

	public CompletionStage<Result> updateLoginNames() {

		RequestBody body = request().body();
		if (body == null) {
			return CompletableFuture.completedFuture(
					badRequest(JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "BAD REQUEST"))));
		}
		JsonNode consumerData = body.asJson();
		ObjectNode consumerUserJson = JsonUtils.newJsonObject();
		ObjectNode loginNameJson = JsonUtils.newJsonObject();
		consumerData.fields().forEachRemaining(field -> {
			if ("EMAIL".equalsIgnoreCase(field.getKey())) {
				loginNameJson.put("loginName", field.getValue().asText());
				loginNameJson.put("type", "EMAIL");
			}
			if ("MOBILE_NUMBER".equalsIgnoreCase(field.getKey())) {
				loginNameJson.put("loginName", field.getValue().asText());
				loginNameJson.put("type", "MOBILE_NUMBER");
			}
		});
		Result sendOtpResult = sendOtp(loginNameJson.get("loginName").asText(), loginNameJson.get("type").asText(), "",
				"").toCompletableFuture().join();
		consumerUserJson.put("source", "updateuser");
		consumerUserJson.set("loginNames", loginNameJson);
		return CompletableFuture.completedFuture(sendOtpResult);

	}

	public CompletionStage<Result> createNewAccFromCheckout() {

		logger.debug("Creating new user from checkout : ");

		ObjectNode consumerDataJson = JsonUtils.newJsonObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		RequestBody body = request().body();
		if (body == null) {
			return CompletableFuture.completedFuture(
					badRequest(JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "BAD REQUEST"))));
		}

		Map<String, String[]> asFormUrlEncoded = body.asFormUrlEncoded();
		ArrayNode consumerArray = consumerDataJson.putArray("loginNames");
		if (MapUtils.isNotEmpty(asFormUrlEncoded)) {
			asFormUrlEncoded.entrySet().forEach(entry -> {
				ObjectNode loginNameJson = JsonUtils.newJsonObject();
				if ("EMAIL".equalsIgnoreCase(entry.getKey())) {
					loginNameJson.put("loginName", entry.getValue()[0]);
					loginNameJson.put("type", "EMAIL");
				}
				if ("MOBILE_NUMBER".equalsIgnoreCase(entry.getKey())) {
					loginNameJson.put("loginName", entry.getValue()[0]);
					loginNameJson.put("type", "MOBILE_NUMBER");
				}
				consumerArray.add(loginNameJson);
			});
			ObjectNode userGroupQuery = JsonUtils.newJsonObject();
			userGroupQuery.put("groupName", "CONSUMER");
			userGroupQuery.put("source", "GETUSERGROUPS");
			String userGroupId = null;
			JsonNode userGroupJson = loginService.getUserGroups(mandatQP, mandatHP, userGroupQuery)
					.toCompletableFuture().join();
			logger.debug("userGroupJson : " + userGroupJson);
			if (JsonUtils.isValidField(userGroupJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)
					&& userGroupJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).size() > 0) {
				JsonNode userGroup = userGroupJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0);

				if (JsonUtils.isValidField(userGroup, UserGroupField.ID.value())) {
					userGroupId = userGroup.get(UserGroupField.ID.value()).asText();
				}
			}

			if (userGroupId == null) {
				return CompletableFuture.completedFuture(internalServerError(
						JsonUtils.getJsonresponseForBadRequest(new ValidationError(UserField.USER_GROUP_ID.value(),
								"USER_GROUP CANNOT BE RETRIVED : " + userGroupQuery))));
			}

			consumerDataJson.put("userGroupId", userGroupId);
			consumerDataJson.put(UserField.LANG.value(), "en");
			consumerDataJson.put("source", "signup");

			return loginService.createNewAccount(mandatQP, mandatHP, consumerDataJson).thenApplyAsync(responseJson -> {
				if (JsonUtils.isValidField(responseJson, "id")) {

					// user created successfully
					session("user_id", responseJson.get("id").asText());
					if (JsonUtils.isValidField(responseJson, SessionTrackingParams.AUTH_TOKEN.getValue())) {
						session(SessionTrackingParams.TEMP_AUTH_TOKEN.getValue(),
								responseJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText());
					}
					if (JsonUtils.isValidField(responseJson, UserAuthField.REFRESH_TOKEN.value())) {
						session(SessionTrackingParams.TEMP_REFRESH_TOKEN.getValue(),
								responseJson.get(UserAuthField.REFRESH_TOKEN.value()).asText());
					}

				}

				logger.debug("session after craeting user : " + session());
				if (JsonUtils.isValidField(responseJson, "id")) {
					return ok(JsonUtils.newJsonObject().put("id", responseJson.get("id").asText()));
				}
				return ok(responseJson);
			}, executionContext.current());

		} else {
			return CompletableFuture.supplyAsync(() -> badRequest("INVALID_REQUEST"));
		}
	}

	public CompletionStage<Result> consumerLoginPage() {
		return CompletableFuture
				.completedFuture(ok(com.arka.consumerportal.sessionweb.views.html.login.render(propUtils))
						.withHeader("Cache-Control", "no-store"));
	}

	public CompletionStage<Result> verifyUserAccount() {
		RequestBody body = request().body();
		if (body == null) {
			return CompletableFuture.completedFuture(
					badRequest(JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "BAD REQUEST"))));
		}

		Map<String, String[]> userRequestMap = body.asFormUrlEncoded();

		String userName = userRequestMap.containsKey("userName")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("userName")).get(0))
						? Arrays.asList(userRequestMap.get("userName")).get(0)
						: null;
		String userType = userRequestMap.containsKey("userType")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("userType")).get(0))
						? Arrays.asList(userRequestMap.get("userType")).get(0)
						: null;
		String flagFromPage = userRequestMap.containsKey("AdditionalField")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("AdditionalField")).get(0))
						? Arrays.asList(userRequestMap.get("AdditionalField")).get(0)
						: null;
		String additionalUserDetail = userRequestMap.containsKey("userDetail")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("userDetail")).get(0))
						? Arrays.asList(userRequestMap.get("userDetail")).get(0)
						: null;
		// System.out.println("-----------"+userName+"flagfrompage--------------"
		// + flagFromPage + "additionaluserDetails-----------"
		// + additionalUserDetail);

		if (StringUtils.isNotEmpty(userName) && StringUtils.isNotEmpty(userType)) {

			ObjectNode isConsumerUserValidJson = JsonUtils.newJsonObject();
			Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
			isConsumerUserValidJson.put("userName", userName);
			isConsumerUserValidJson.put("source", "verifyuser");
			return loginService.verifyUserAccount(mandatQP, mandatHP, JsonUtils.toJson(isConsumerUserValidJson))
					.thenComposeAsync(userJson -> {
						if (isResponseNotNull(userJson) && isUserExists(userJson) && isUserActive(userJson)) {

							if (StringUtils.isNotEmpty(flagFromPage)) {
								return sendOtp(userName, userType, flagFromPage, additionalUserDetail);
							} else {
								return sendOtp(userName, userType, "", "");
							}
						} else {
							Map<String, String> errorMap = new HashMap<>();
							if (JsonUtils.isValidField(userJson, "status")
									&& "INVALID".equalsIgnoreCase(userJson.get("status").asText())) {
								errorMap.put("error", langMsg(UserMessageKey.USER_STATUS_INACTIVE));
							} else if (JsonUtils.isValidField(userJson, "status")
									&& "RESOURCE_NOT_FOUND".equals(userJson.get("status").asText())) {
								errorMap.put("error", userJson.get("status").asText());
							} else if (JsonUtils.isValidField(userJson, "error")) {
								errorMap.put("error", userJson.get("error").asText());
							} else {
								errorMap.put("error", "error");
							}
							return CompletableFuture.completedFuture(ok(JsonUtils.toJson(errorMap)));
						}
					}, executionContext.current());
		} else {
			return CompletableFuture.completedFuture(badRequest(
					JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "INVALID REQUEST"))));
		}
	}

	public CompletionStage<Result> sendOtp() {
		RequestBody body = request().body();
		logger.debug("Send OTP request recived : " + request().body());
		if (body == null) {
			return CompletableFuture.completedFuture(
					badRequest(JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "BAD REQUEST"))));
		}
		Map<String, String[]> userRequestMap = body.asFormUrlEncoded();
		String userName = userRequestMap.containsKey("userName")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("userName")).get(0))
						? Arrays.asList(userRequestMap.get("userName")).get(0)
						: null;
		String userType = userRequestMap.containsKey("userType")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("userType")).get(0))
						? Arrays.asList(userRequestMap.get("userType")).get(0)
						: null;
		String otpSource = userRequestMap.containsKey("source")
				&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("source")).get(0))
						? Arrays.asList(userRequestMap.get("source")).get(0)
						: null;
		if (("misp").equals(otpSource)) {
			return sendOtp(userName, userType, otpSource, "");
		} else {
			String existingUserName = userRequestMap.containsKey("existingUserName")
					&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("existingUserName")).get(0))
							? Arrays.asList(userRequestMap.get("existingUserName")).get(0)
							: null;
			if (StringUtils.isNotEmpty(existingUserName)) {
				logger.debug("OTP request recived from my profile ");
				if (!session().containsKey(SessionTrackingParams.USER_ID.getValue())) {
					logger.debug("User had logged out of the session , therefore unable to generate OTP");
					return CompletableFuture.completedFuture(ok(JsonUtils.errorDataBadRequestJson("USER ID NULL")));
				}
			}
		}
		return sendOtp(userName, userType, "", "");

	}

	private CompletionStage<Result> sendOtp(String userName, String userType, String otpSource,
			String additionalUserDetail) {
		if (StringUtils.isNotEmpty(userName) && StringUtils.isNotEmpty(userType)) {
			RequestBody body = request().body();
			if (body == null) {
				return CompletableFuture.completedFuture(badRequest(
						JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "BAD REQUEST"))));
			}
			Map<String, String[]> userRequestMap = body.asFormUrlEncoded();

			ObjectNode sendOTPJson = JsonUtils.newJsonObject();
			Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
			sendOTPJson.put("userName", userName);
			sendOTPJson.put("userType", userType);
			sendOTPJson.put("source", "sendotp");
			sendOTPJson.put("otpSource", otpSource);

			String existingUserName = userRequestMap.containsKey("existingUserName")
					&& StringUtils.isNotBlank(Arrays.asList(userRequestMap.get("existingUserName")).get(0))
							? Arrays.asList(userRequestMap.get("existingUserName")).get(0)
							: null;

			// String existingUserName = queryString.get("existingUserName") !=
			// null
			// && Arrays.asList(queryString.get("existingUserName")).size() > 0
			// ? queryString.get("existingUserName")[0] : null;
			sendOTPJson.put("existingUserName", existingUserName);
			if (StringUtils.isNotEmpty(additionalUserDetail)) {
				sendOTPJson.put("additionalUserDetail", additionalUserDetail);
			}
			return loginService.sendOtp(mandatQP, mandatHP, sendOTPJson).thenApplyAsync(responseJson -> {
				logger.debug("otp response is : " + responseJson);
				ObjectNode newRespObj = (ObjectNode) responseJson;
				if (isResponseNotNull(newRespObj) && newRespObj.get("id") != null) {
					newRespObj.put("success", langMsg(UserMessageKey.USER_OTP_SENT));
					return ok(newRespObj);
				} else {
					if (!JsonUtils.isValidField(newRespObj, UserMessageKey.USR_RESP_STATUS.value())) {
						newRespObj.put(UserMessageKey.USER_STATUS.value(), langMsg(UserMessageKey.USER_ACC_NOT_FOUND));
					}
					return ok(newRespObj);
				}
			}, executionContext.current());

		} else {
			return CompletableFuture.completedFuture(badRequest(
					JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "INVALID REQUEST"))));
		}
	}

	public CompletionStage<Result> verifyOtp() {

		RequestBody body = request().body();
		if (body == null) {
			return CompletableFuture.completedFuture(
					badRequest(JsonUtils.getJsonresponseForBadRequest(new ValidationError("ERROR", "BAD REQUEST"))));
		}

		logger.debug(body.asText());

		Map<String, String[]> asFormUrlEncoded = body.asFormUrlEncoded();

		logger.debug("asFormUrlEncoded : " + asFormUrlEncoded);

		ObjectNode verifyOTPJson = JsonUtils.newJsonObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		String visitorId = mandatHP.get("x-visitor-id").get(0);
		String otpSource = asFormUrlEncoded.get("source")[0];
		verifyOTPJson.put("userName", asFormUrlEncoded.get("userName")[0]);
		verifyOTPJson.put("otp", asFormUrlEncoded.get("OTP")[0]);
		verifyOTPJson.put("otpSource", otpSource);
		String existingUserName = asFormUrlEncoded.get("existingUserName") != null
				&& Arrays.asList(asFormUrlEncoded.get("existingUserName")).size() > 0
						? asFormUrlEncoded.get("existingUserName")[0]
						: "";
		String userType = asFormUrlEncoded.get("userType") != null
				&& Arrays.asList(asFormUrlEncoded.get("userType")).size() > 0 ? asFormUrlEncoded.get("userType")[0]
						: "";
		if (StringUtils.isNotEmpty(existingUserName)) {
			logger.debug("OTP verification request recived from my profile ");
			if (!session().containsKey(SessionTrackingParams.USER_ID.getValue())) {
				logger.debug("User had logged out of the session , therefore unable to verify OTP");
				return CompletableFuture.completedFuture(ok(JsonUtils.errorDataBadRequestJson("USER ID NULL")));
			}
		}

		if (StringUtils.isNotEmpty(userType) && StringUtils.isNotEmpty(existingUserName)) {
			verifyOTPJson.put("existingUserName", existingUserName);
			verifyOTPJson.put("userType", userType);
			verifyOTPJson.put("sessionId", session(SessionTrackingParams.SESSION_ID.getValue()));
		}
		verifyOTPJson.put("source", "verifyotp");
		return loginService.verifyOtp(mandatQP, mandatHP, verifyOTPJson).thenApplyAsync(respJson -> {
			if (!JsonUtils.isValidField(verifyOTPJson, "existingUserName")) {
				setUserSession(respJson);
			}
			if (JsonUtils.isValidField(respJson, "sessionId") && StringUtils.isNotBlank(respJson.get("sessionId").asText())) {
				if (("checkout").equals(otpSource)) {
					cache.getOrElse(CacheService.getCacheKey("checkout" + "." + respJson.get("sessionId").asText(), null, "otpVerified"),
							RedisDB.PURCHASE.getValue(), () -> {
								return JsonUtils.stringify(true);
							});
				} 
			}else {
				if (("checkout").equals(otpSource) && JsonUtils.isValidField(verifyOTPJson, "sessionId")) {
					cache.getOrElse(CacheService.getCacheKey("checkout" + "." + verifyOTPJson.get("sessionId").asText(), null, "otpVerified"),
							RedisDB.PURCHASE.getValue(), () -> {
								return JsonUtils.stringify(true);
							});
				}
			}

			// session().put("check", "session");
			return ok(respJson);
		}, executionContext.current());
	}

	public CompletionStage<Result> signout() {
		String queryString = request().getQueryString("logout");
		ObjectNode bodyJson = JsonUtils.newJsonObject();
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		bodyJson.put("sessionId", session().get(SessionTrackingParams.SESSION_ID.getValue()));
		bodyJson.put("source", "logout");
		return loginService.signout(mandatQP, mandatHP, bodyJson).thenApplyAsync((json) -> {
			boolean isMisp = false;
			if (session().containsKey(SessionTrackingParams.MISP_ID.getValue())) {
				isMisp = true;
			}
			session().clear();
			if (StringUtils.isNotEmpty(queryString) && "true".equalsIgnoreCase(queryString)) {
				session("logout", "true");
			}
			if (isMisp) {
				((ObjectNode) json).set("misplogout", Json.toJson("true"));
			}
			return ok(json);
		}, executionContext.current());
	}

	private boolean isResponseNotNull(JsonNode responseJson) {
		return responseJson != null;
	}

	private boolean isUserExists(JsonNode respJson) {
		return JsonUtils.isValidField(respJson, "status")
				&& !"RESOURCE_NOT_FOUND".equals(respJson.get("status").asText());
	}

	private boolean isUserActive(JsonNode respJson) {
		return JsonUtils.isValidField(respJson, "status") && "Active".equalsIgnoreCase(respJson.get("status").asText());
	}

	private boolean hasUserSession(JsonNode respJson) {
		return JsonUtils.isValidField(respJson, "id") && JsonUtils.isValidField(respJson, "sessionId")
				&& JsonUtils.isValidField(respJson, "userName");
	}

	private void setUserSession(JsonNode responseJson) {

		logger.debug("Response for setting user session : " + Json.prettyPrint(responseJson));

		if (hasUserSession(responseJson)) {
			ObjectNode consumerProfileData = JsonUtils.newJsonObject();
			Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

			logger.debug("context args while setting session : " + Context.current().args);

			consumerProfileData.put("source", "getuserprofilename");
			consumerProfileData.put("userId", responseJson.get("id").asText());

			session("uid", responseJson.get("id").asText());
			session("sid", responseJson.get("sessionId").asText());
			session("userName", responseJson.get("userName").asText());
			// session("lastUpdatedTime",
			// dateFormatofLastUpdatedTime(responseJson.get("lastUpdatedTime").asLong()));

			// set authToken and refreshToken in session & remove temp
			// authentication
			String authToken = "";
			String refreshToken = "";

			if (JsonUtils.isValidField(responseJson, SessionTrackingParams.AUTH_TOKEN.getValue())) {
				authToken = responseJson.get(SessionTrackingParams.AUTH_TOKEN.getValue()).asText();
				session(SessionTrackingParams.AUTH_TOKEN.getValue(), authToken);
				Context.current().args.put(SessionTrackingParams.AUTH_TOKEN.getValue(), authToken);
			}
			if (JsonUtils.isValidField(responseJson, UserAuthField.REFRESH_TOKEN.value())) {
				refreshToken = responseJson.get(UserAuthField.REFRESH_TOKEN.value()).asText();
				session(UserAuthField.REFRESH_TOKEN.value(), refreshToken);
				Context.current().args.put(UserAuthField.REFRESH_TOKEN.value(), refreshToken);
			}

			logger.debug("Session with verify OTP : " + session());

			logger.debug("Fetching profile name : " + responseJson.get("userName").asText());

			// since it is temp auth url we send tempAuthToken
			mandatHP.put(SessionTrackingParams.AUTH_TOKEN.getValue(), Arrays.asList(authToken));
			mandatHP.put(UserAuthField.REFRESH_TOKEN.value(), Arrays.asList(refreshToken));
			mandatHP.put(SessionTrackingParams.USER_ID.getValue(), Arrays.asList(responseJson.get("id").asText()));

			mandatHP.put(SessionTrackingParams.USER_ID.getValue(), Arrays.asList(responseJson.get("id").asText()));

			JsonNode profileJson = profileService.manageUserAccount(mandatQP, mandatHP, consumerProfileData)
					.toCompletableFuture().join();

			logger.info("profileJson : " + Json.prettyPrint(profileJson));

			if (JsonUtils.isValidField(profileJson, UserProfileField.USER_FULL_NAME.value())) {
				session("profileName", profileJson.get(UserProfileField.USER_FULL_NAME.value()).asText());
			}
		}
	}

	private String dateFormatofLastUpdatedTime(Long dateValue) {
		Date date = new Date(dateValue);
		SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
		String dateText = df2.format(date);
		return dateText;
	}

	private String langMsg(UserMessageKey userStatusInactive) {
		return messagesApi.get(Http.Context.current().lang(), userStatusInactive.value());
	}
}
