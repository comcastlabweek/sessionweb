package com.arka.consumerportal.sessionweb.services.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.services.SessionServ;
import com.arka.consumerportal.sessionweb.services.LoginService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

public class LoginServiceImpl implements LoginService {
	@Inject
	SessionServ sessionServ;
	
	@Override
	public CompletionStage<JsonNode> generateUniqueId(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.generateUniqueId(queryString, header, bodyJson);
	}
	
	@Override
	public CompletionStage<JsonNode> accountType(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.accountType(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> signout(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.signout(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> isUserExist(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.isUserExist(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> verifyUserAccount(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.verifyUserAccount(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> sendOtp(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.sendOtp(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> verifyOtp(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.verifyOtp(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> createPwd(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.createPwd(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> resetPwd(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.resetPwd(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> createNewAccount(Map<String, List<String>> queryString,
			Map<String, List<String>> header, JsonNode bodyJson) {
		return sessionServ.createNewAccount(queryString, header, bodyJson);
	}

	@Override
	public CompletionStage<JsonNode> getUser(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson) {
		return sessionServ.getUser(queryString, header, bodyJson);
	}
	
	@Override
	public CompletionStage<JsonNode> getUserGroups(Map<String, List<String>> queryString,
			Map<String, List<String>> header,JsonNode bodyJson) {
		return sessionServ.getUserGroups(queryString, header, bodyJson);
	}

}
