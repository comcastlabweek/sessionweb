package com.arka.consumerportal.sessionweb.messagekey.utils;

public enum UserMessageKey {

	USER_NAME_MANDATORY("user.username.required"),

	USER_STATUS_INACTIVE("user.status.inactive"),

	USER_OTP_SENT("user.otp.sent"),
	
	USR_RESP_STATUS("status"),
	
	USER_STATUS("error"),

	USER_ACC_NOT_FOUND("user.accout.not.found"),

	MOBILE_NUMBER_MANDATORY("user.mobile.number.required"),

	MOBILE_NUMBER_MAX_LENGTH("user.mobile.number.maxlength"),

	EMAIL_MANDATORY("user.email.required"),

	EMAIL_INVALID("user.email.invalid"),

	USER_TYPE_MANDATORY("user.type.required"),

	USER_TYPE_INVALID("user.type.invalid"),

	PASSWORD_MANDATORY("user.password.required"),

	USER_ID_MANDATORY("user.id.required"),

	LOCALE_MANDATORY("user.locale.required"),

	LANG_NOT_RESOLVED("user.lang.not.resolved"),

	DEACTIVATION_SOURCE_MANDATORY("user.deactivation.source.required"),

	DEACTIVATION_REASON_MANDATORY("user.deactivation.reason.required"),

	DEACTIVATION_SOURCE_INVALID("user.deactivation.source.invalid"),

	NUMBER_OF_ROWS_LESS_THAN_1("numrows.required.lessthan.0"),

	PAGE_NUMBER_LESS_THAN_0("pagenumber.lessthan.0"),

	USER_CREDENTIAL_MISMATCH("user.credential.mismatch"),

	PLS_TRY_AGAIN("pls.try.again"),
	
	MOBILE_NUMBER_ALREADY_EXIST("user.field.error.mobile.exist"),
	
	EMAIL_ID_ALREADY_EXIST("user.field.error.email.exist");

	private String value;

	private UserMessageKey(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
