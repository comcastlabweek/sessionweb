package com.arka.consumerportal.sessionweb.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.consumerportal.sessionweb.services.impl.LoginServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

@ImplementedBy(LoginServiceImpl.class)
public interface LoginService {

	CompletionStage<JsonNode> generateUniqueId(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> accountType(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);

	CompletionStage<JsonNode> signout(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> isUserExist(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> verifyUserAccount(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> sendOtp(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> verifyOtp(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> createPwd(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> resetPwd(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> createNewAccount(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	
	CompletionStage<JsonNode> getUser(Map<String, List<String>> queryString, Map<String, List<String>> header,
			JsonNode bodyJson);
	CompletionStage<JsonNode> getUserGroups(Map<String, List<String>> queryString, Map<String, List<String>> header,JsonNode bodyJson);
	
}
